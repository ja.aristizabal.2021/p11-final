# ENTREGA CONVOCATORIA ENERO
Joan Andreu Aristizábal Guillén:       ja.aristizabal.2021@alumnos.urjc.es

## Parte básica
La ejecución del programa se realiza en el orden que se establece en el enunciado y sus respectivos argumentos: 
signaling.py -> streamer.py -> front.py . Se requerirá que para la ejecución del programa estén en el mismo directorio:
signaling.py, streamer.py, front.py, client.js, index.html y el fichero de video.

El servidor frontal maneja la señalización del navegador, que lo forman los ficheros : client.js e index.html . 

El funcionamiento de esta práctica es el siguiente: el servidor de señalización mostrará un mensaje de inicio por pantalla 
al arrancarse, el streamer se registrará subiendo un fichero de video que el s. señalización guardará en una lista, y el 
servidor frontal realizará la petición de la lista de videos disponibles al servidor signaling. Al recibirla, se mostrará en el navegador un botón por cada video 
disponible y al pulsar en el deseado, front informará al servidor de señalización del nombre del video y enviará la descripción local del navegador al mismo,
que emparejará al streamer con ese servidor frontal en un diccionario para que distinguir sus direcciones IP y puertos a la 
de intercambiar los sdp de oferta y respuesta. Cuando esto último haya ocurrido, finalmente se verá el video en el navegador.

Video de demostración: https://youtu.be/czqD0S3i4U0

Observaciones:

El formato de los mensajes históricos es el que indica el enunciado. Sin embargo, hay más tipos de mensajes a parte de los
que se sugieren. Además, el formato especificado con el tiempo se ha usado en los tres ficheros python.

Se ha añadido en el repositorio un video para probar el funcionamiento del programa, video2.mp4 .

El fichero html con el que se crea el navegador es modified_index.html, pero está hecho a partir de una modificación de 
index.html. Ambos están incluidos en el repositorio.

En el código de los ficheros streamer.py y front.py, además de la parte básica, se han incluido dos lineas que crean un
fichero de texto en modo lectura que almacenará el sdp con la descripción local del propio servidor. Una parte que se 
pedía en el enunciado para realizar las capturas.

En el segundo escenario propuesto para realizar las capturas, en el enunciado, mientras se visualiza el video 
en el primer navegador y habiendo finalizado no se llega a reproducir el video en el segundo navegador. Sin embargo, si se llegan a hacer los dos recursos GET y un POST, además de los sdp de oferta y respuesta que intercambian 
tanto del streamer como de servidor frontal. Esto último puede comprobarse en la captura captura2.libcap .


