import argparse
import asyncio
import json
import logging
import os
import platform
import ssl
import time

from aiohttp import web
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRelay
from aiortc.rtcrtpsender import RTCRtpSender
import ast # modulo instalado para reconvertir lista de videos

ROOT = os.path.dirname(__file__)


relay = None
webcam = None


initialValue = 'value'      # valor inicial de las variables globales
desc = initialValue         # Almacenara la descripcion local del sdp
sdpAnswer = initialValue    # Almacenara la descripcion remota
videoList = initialValue
cliente = None
transport = None
address = None              # Almacenara la direccion del servidor de senyal
port = None                 # Almacenara el puerto del servidor de senyal
contador = 0


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frame_count = 0  # Agrega un contador de frames


    def connection_made(self, transport):
        current_time = time.time()
        local_time = time.localtime(current_time)
        now = time.strftime("%Y%m%d%H%M%S", local_time)
        self.transport = transport
        print(f"{now} Comenzando")
        print(f'{now} Enviando :', self.message)
        self.transport.sendto(self.message.encode())


    def datagram_received(self, data, addr):
        current_time = time.time()
        local_time = time.localtime(current_time)
        now = time.strftime("%Y%m%d%H%M%S", local_time)
        try:
            message = json.loads(data.decode())
        except:
            message = data.decode()
            pass
        if 'sdp' in message:
            print(f"{now} Mensaje recibido de {addr}", data.decode())
            answer = json.loads(data.decode())              # Extrayendo answer como diccionario
            sdp = answer['sdp']
            type = answer['type']
            global sdpAnswer
            sdpAnswer = RTCSessionDescription(sdp, type)    # Instanciando la descripcion remota como RTC
        else :
            print(f"{now} Mensaje recibido de {addr}", data.decode())
            if data.decode() == '{"type": "bye"}':
                global recvBye
                recvBye = True
                print(f'{now} Cerrando...')
            else:
                global videoList
                videoList = data.decode()


    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


# Funcion que espera a recibir el mensaje con la descripcion remota
async def wait_for_an_answer():
    global sdpAnswer
    while sdpAnswer == initialValue:
        await asyncio.sleep(1)

# Funcion que espera a recibir la lista de videos
async def wait_for_video_list():
    global videoList
    while videoList == initialValue:
        await asyncio.sleep(1)

def force_codec(pc, sender, forced_codec):
    kind = forced_codec.split("/")[0]
    codecs = RTCRtpSender.getCapabilities(kind).codecs
    transceiver = next(t for t in pc.getTransceivers() if t.sender == sender)
    transceiver.setCodecPreferences(
        [codec for codec in codecs if codec.mimeType == forced_codec]
    )

# Funcion que ofrece la pagina web al navegador
async def index(request):
    global cliente, transport, videoList, address, port
    # Transporte UDP
    mensaje = "PETICION:"
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    cliente = EchoClientProtocol(mensaje, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: cliente, remote_addr=(address, port))
    await wait_for_video_list()

    # Modifica el html para anyadir botones por cada video
    with open('index.html', 'r') as indexHtml:      # lee el fichero ya creado
        lines = indexHtml.readlines()

    for i, line in enumerate(lines):
        if '<button id="start" onclick="start()">Start</button>' in line:
            videoListlist = ast.literal_eval(videoList)
            for iVideo in videoListlist:
                newLine = line.replace('<button id="start" onclick="start()">Start</button>',
                                       '<button id="start ' + iVideo + '" onclick="start(\''+iVideo+'\')">' + iVideo + '</button>' + '\n' +
                                       '<button id="stop ' + iVideo + '"style="display: none" onclick="stop(\''+iVideo+'\')">Stop</button>')
                lines[i] = newLine
                i = i + 1

    with open('modified_index.html', 'w') as fileHtml:   # Crea un nuevo fichero con los cambios sobre el otro html
        fileHtml.writelines(lines)
        # fileHtml.close()
    with open('modified_index.html', 'r') as fileHtml:
        fileHtml = fileHtml.read()

    # Ofrecer respuesta al navegador
    return web.Response(content_type="text/html", text=fileHtml)

# Funcion que va a realizar la conexion webrtc
async def javascript(request):
    content = open(os.path.join(ROOT, "client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)


# Funcion que maneja la senyalizacion con el streamer
async def offer(request):
    current_time = time.time()
    local_time = time.localtime(current_time)
    now = time.strftime("%Y%m%d%H%M%S", local_time)
    params = await request.json()                                           # Oferta recibida del navegador
    print(f'{now} Oferta recibida: {params}')
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
    global desc, cliente, transport, contador
    desc = json.dumps(offer.__dict__)
    contador = contador + 1
    with open(f'2_navegador-{contador}.sdp', 'w') as file:            # Guardando descripcion local en un fichero
        file.write(offer.sdp)
    cliente.transport.sendto(f'NAME:{params["video"]}'.encode())
    cliente.transport.sendto(desc.encode())
    print(f'{now} Enviando solicitud de reproduccion para el video: {params["video"]}')
    print(f'{now} Enviando oferta: {desc}')
    await wait_for_an_answer()
    return web.Response(
        content_type="application/json",
        text=json.dumps(
            {"sdp": sdpAnswer.sdp, "type": sdpAnswer.type}
        ),
    )
pcs = set()

# Funcion que cierra peer connections
async def on_shutdown(app):
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()


def main():
    parser = argparse.ArgumentParser(description="WebRTC webcam demo")
    parser.add_argument("httpport", type=int, help="Port for HTTP server (default: 8080)")  # Puerto http
    parser.add_argument("signalIp", type=str)       # IP del servidor de senyalizacion
    parser.add_argument("signalPort", type=int)     # Puerto del servidor de senyalizacion
    parser.add_argument("--host", default="0.0.0.0")
    parser.add_argument("--verbose", "-v", action="count")


    args = parser.parse_args()
    global address, port
    address = args.signalIp
    port = args.signalPort
    logging.basicConfig(level=logging.INFO)
    ssl_context = None

    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/client.js", javascript)
    app.router.add_post("/offer", offer)
    web.run_app(app, host=args.host, port=args.httpport, ssl_context=ssl_context)

if __name__ == "__main__":
    main()
