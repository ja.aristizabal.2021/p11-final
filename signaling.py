import asyncio
import json
import time
import argparse

register={}     # Diccionario de clientes conectados con el servidor
streamers={}    # Diccionario de streamers
partners = {}   # parejas de fronts con streames
videos = []     # Listado de videos disponibles



class ServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        current_time = time.time()
        local_time = time.localtime(current_time)
        message = data.decode()
        head = message.split(':')
        if head[0] == "VIDEO":      # Mensaje con nombre del video (streamer)
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Mensaje {head[0]} recibido de ', (addr))
            videoName = head[1]
            streamers[videoName] = addr
            videos.append(videoName)
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Servidores disponibles :', streamers)
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Videos disponibles :', videos)

        elif head[0] == "PETICION":     # Peticion de listas de videos (front)
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Mensaje {head[0]} recibido de ', (addr))
            videosStr = str(videos)
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Enviando lista de videos a ({addr}):', videos)
            self.transport.sendto(videosStr.encode(), addr)

        elif head[0] == "NAME":         # Mensaje con nombre del video que quiere ver el front
            print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Mensaje {head[0]} recibido de ', (addr))
            streamAddr = head[1]
            partners[addr] = streamers[streamAddr]
        else:                           # Deteccion de mensajes sdp offer/answer
            messageSdp = json.loads(data.decode())
            tipo = messageSdp['type']
            if tipo == 'offer':
                print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Mensaje de oferta recibido de ', addr)
                self.transport.sendto(message.encode(), partners[addr])
                print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Enviando mensaje de oferta a ', partners[addr])

            elif tipo == 'answer':
                print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Mensaje de respuesta recibido de ', addr)
                front = next(k for k, v in partners.items() if v == addr)
                print(f'{time.strftime("%Y%m%d%H%M%S", local_time)} Enviando mensaje de respuesta a ', front)
                self.transport.sendto(message.encode(), front)



async def main():
    parser = argparse.ArgumentParser(description="Argumentos para el servidor de senyal")
    parser.add_argument('port', type=int, help='Nombre del archivo de entrada')  # puerto
    args = parser.parse_args()
    port= args.port

    current_time = time.time()
    local_time = time.localtime(current_time)
    now = time.strftime("%Y%m%d%H%M%S", local_time)
    print(f"{now} Comenzando")

    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ServerProtocol(),
        local_addr=('127.0.0.1', port))

    try:
        await asyncio.sleep(3600)
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())