import argparse
import asyncio
import logging
import math
import json
import ffmpeg
import time
import cv2
import numpy
from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling
from av import VideoFrame

initialValue = 'value'      # valor inicial de las variables globales
sdpOffer = initialValue     # Almacenara la descripcion remota
lastMessage = initialValue  # Almacenara el ultimo mensaje recibido
counter = 0
videoName = initialValue    # Almacenara el nombre del video

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frame_count = 0        # Contador de frames

    def connection_made(self, transport):
        current_time = time.time()
        local_time = time.localtime(current_time)
        now = time.strftime("%Y%m%d%H%M%S", local_time)
        self.transport = transport
        print(f"{now} Comenzando")
        print(f'{now} Subiendo video: {self.message}')
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        current_time = time.time()
        local_time = time.localtime(current_time)
        now = time.strftime("%Y%m%d%H%M%S", local_time)
        global lastMessage
        lastMessage = data.decode()
        print(f"{now} Recibido mensaje de oferta de {addr}: ", data.decode())
        answer = json.loads(data.decode())      # Extrayendo offer como diccionario
        sdp = answer['sdp']
        tipo = answer['type']
        global sdpOffer
        sdpOffer = RTCSessionDescription(sdp, tipo)     # Instanciando la descripcion remota como RTC

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


# Funcion que espera a recibir el mensaje con la descripcion remota
async def wait_for_an_offer():
    while sdpOffer == initialValue:
        await asyncio.sleep(1)

# Funcion que espera por la descripcion remota
async def wait_for_remote_description(pc):

    while not pc.remoteDescription is None:
        await asyncio.sleep(1)

class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1
        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr

# Funcion que maneja la senyalizacion con el servidor de senyal y que enviara el video al navegador
async def run(pc, player, signalIp, signalPort):
    async def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    name = videoName                      # Nombre del archivo de video
    registerMessage = 'VIDEO:' + name       # Mensaje de registro

    cliente = EchoClientProtocol(registerMessage, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: cliente, remote_addr=(signalIp, int(signalPort)))

    # Funcion que maneja la senyalizacion con el servidor UDP
    async def consumSignalling(pc, cliente):

            global sdpOffer, lastMessage, counter
            current_time = time.time()
            local_time = time.localtime(current_time)
            now = time.strftime("%Y%m%d%H%M%S", local_time)
            if lastMessage == initialValue:
                await wait_for_an_offer()                               # Esperando por la descripcion remota
                await pc.setRemoteDescription(sdpOffer)
                await add_tracks()
                await pc.setLocalDescription(await pc.createAnswer())   # Creando la descripcion local
                sdpAnswerDict = pc.localDescription.__dict__
                sdpAnswer = json.dumps(sdpAnswerDict)
                counter = counter + 1
                with open(f'2_streamer-{counter}.sdp', 'w') as file:   # Guardando descripcion local en un fichero
                    file.write(pc.localDescription.sdp)
                cliente.transport.sendto(sdpAnswer.encode())  # Enviando answer
                print(f'{now} Enviando mensaje de respuesta : ', sdpAnswer)
                await wait_for_remote_description(pc)         # Esperando a establecer la conexion con el cliente
            print()

    await consumSignalling(pc, cliente)
    try:
        await on_con_lost
    finally:
        transport.close()


def main():
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument('file', type= str, help='Nombre del archivo de entrada')
    parser.add_argument("signalip", type=str, help="Numero entero")
    parser.add_argument("signalport", type=str, help="Numero entero")
    parser.add_argument("--verbose", "-v", action="count")

    add_signaling_arguments(parser)
    args = parser.parse_args()

    # crea la peer connection
    pc = RTCPeerConnection()

    #   asignammos argumentos a las variables globales
    global videoName
    if args.file:
        videoName = args.file
        player = MediaPlayer(args.file)     # introducimos el nombre del archivo de video
    if args.signalip:
        direction = args.signalip
    if args.signalport:
        port = args.signalport

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc= pc,
                player=player,
                signalIp = direction,
                signalPort = port,
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(pc.close())



if __name__ == "__main__":
    main()